#ifndef __BOARD_H__
#define __BOARD_H__

#include <bitset>
#include <vector>
#include <iostream>
#include "common.h"
using namespace std;

class Board {
   
private:
    bitset<64> black;
    bitset<64> taken;    
       
    void set(Side side, int x, int y);
    bool onBoard(int x, int y);

    int score;
      
public:
    Board();
    ~Board();
    Board *copy();

    bool get(Side side, int x, int y);
    bool occupied(int x, int y);
        
    bool isDone();
    bool hasMoves(Side side);
    bool checkMove(Move *m, Side side);
    void doMove(Move *m, Side side);
    int count(Side side);
    int countBlack();
    int countWhite();
    int countTotal();

    int countMoves(Side side);
    vector<Move*>* getMoves(Side side);

    void setBoard(char data[]);
    void printBoard();
};

#endif
