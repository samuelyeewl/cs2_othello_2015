public class MultiTestGame {

    public static void main(String[] args) {
        if (args.length != 3 && args.length != 4) {
            // Wrong number of arguments; print usage string.
            String s
                = "\nUsage: TestGame BLACK WHITE NUMRUNS[TIMELIMIT]\n"
                + "  BLACK/WHITE - c++ program name or:\n"
                + "    SimplePlayer, ConstantTimePlayer, BetterPlayer - AIs\n"
                + "    Human - manual input\n"
                + "  NUMRUNS - number of runs\n"
                + "  TIMELIMIT - optional timelimit for each player in milliseconds:\n";                           
            System.out.println(s);
            System.exit(-1);
        }

        // Get the number of runs
        int num_runs = 1;
        try {
            num_runs = Integer.parseInt(args[2]);
        } catch (Exception e) {
            System.out.println("Error: Could not parse integer from '" + args[2] + "'");
            System.exit(-1);
        }
        
        // parse the timeout
        long timeout = 0;
        if (args.length != 3) {
            // Parse timeout arg.       
            try {
                timeout = Integer.parseInt(args[3]);                                  
            } catch (Exception e) {
                System.out.println("Error: Could not parse integer from '" + args[3] + "'");
                System.exit(-1);
            }
        }

        // Start the game.
        OthelloMultiObserver o = new OthelloMultiObserver(num_runs);
        OthelloGame g = null;

        // Run the game
        for (int i = 0; i < num_runs; ++i) {
            // Create new players
            int which = 1;
            OthelloPlayer[] players = new OthelloPlayer[2];
            for (int j = 0; j < 2; j++) {
                if (args[j].equalsIgnoreCase("SimplePlayer")) {
                    players[j] = new SimplePlayer();
                } else if (args[j].equalsIgnoreCase("ConstantTimePlayer")) {
                    players[j] = new ConstantTimePlayer();
                } else if (args[j].equalsIgnoreCase("BetterPlayer")) {
                    players[j] = new BetterPlayer();
                } else if (args[j].equalsIgnoreCase("Human")) {
                    players[j] = new OthelloDisplay(which++);
                } else {
                    players[j] = new WrapperPlayer(args[j]);
                }             
            }

            // Run the game
            System.out.println("Starting game, Run " + (i+1) + "/" + num_runs);
            if (timeout == 0) {
                g = new OthelloGame(players[0], players[1], o);
            } else {
                g = new OthelloGame(players[0], players[1], o, timeout);  
            }

            g.run();
        }

        o.PrintResults();
    }
}
