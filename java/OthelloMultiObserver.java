/**
 * An observer that records the win statistics over multiple runs
 * <p>
 * $Id: OthelloTextObserver.java,v 1.6 2005/02/17 07:56:11 plattner Exp $
 *
 * @author Aaron Plattner
 **/
 
public class OthelloMultiObserver implements OthelloObserver
{
   /** The player who's move is next.
    **/
   // private OthelloSide player = OthelloSide.BLACK;

   private OthelloResult[] results;
   private int current_run;
   private int runs;

   public OthelloMultiObserver(int num_runs)
   {
      super();
      results = new OthelloResult[num_runs];
      current_run = 0;
      runs = num_runs;
   }

   public void OnMove(Move m, long blackTimeout, long whiteTimeout)
   {
      // System.out.println(player + ": " + m);
      // System.out.println("Time left: black: " + OthelloUtil.showTime(blackTimeout) + " white: " + OthelloUtil.showTime(whiteTimeout));
      // player = player.opposite();
   }

   public void OnGameOver(OthelloResult r)
   {
      System.out.println("Game result: " + r);

      // If there was a runtime error, print the stack trace
      if(r.error instanceof ErrorException)
      {
         ErrorException e = ((ErrorException)r.error);
         Throwable playerException = e.error;
         playerException.printStackTrace();
      }

      // store r
      if (current_run < runs) {
         results[current_run] = r;
         current_run++;
      }
   }

   public void PrintResults()
   {
      int black_wins = 0;
      int black_score = 0;
      int white_wins = 0;
      int white_score = 0;

      for (OthelloResult result : results) {
         if (result.getWinner() == OthelloSide.BLACK)
            black_wins++;
         else if (result.getWinner() == OthelloSide.WHITE)
            white_wins++;

         black_score += result.blackScore;
         white_score += result.whiteScore;
      }

      System.out.println("Final Results, " + runs + " games:");
      System.out.println("Black: " + black_wins + " wins, Average score: " + (black_score/runs));
      System.out.println("White: " + white_wins + " wins, Average score: " + (white_score/runs));
      System.out.println("Ties: " + (runs - black_wins - white_wins));
   }
}
