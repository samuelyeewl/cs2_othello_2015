#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include <climits>
#include <cstring>
#include <queue>
#include <sys/time.h>
#include "common.h"
#include "board.h"
#include "heuristic.h"
using namespace std;

#define OPPSIDE(side)	((side) == WHITE ? BLACK : WHITE)
#define MAX_DEPTH 20
#define INITIAL_DEPTH 3
#define DEBUG false
#define TWOD(arr, x, y) (arr[(x) + 8*(y)])

typedef pair<Move*, int> MoveScore;
struct HigherMoveScore
{
	bool operator()(const MoveScore& left, const MoveScore& right) const {
		return left.second < right.second;
	}
};
typedef priority_queue<MoveScore, vector<MoveScore>, HigherMoveScore> MoveQueue;

class Player {
private:
	Board* current_board;
	Side s;

	timeval starttime;
	timeval currenttime;

	// A table that contains the weight of each square.
	int weight_table[8*8];

	/*
	 * Heuristic functions
	 */
    // 1) Constant score heuristic
    int constant_score(Board* board, Side side);
    // 2) Greedy heuristic (best score at end of move)
    int greedy_score(Board* board, Side side);
    // 3) Positional heuristic (prefer corners and edges, avoid squares adjacent to corners and edges)
    int positional_score(Board* board, Side side);
    // 4) Mobility score (prefer moves that give us future mobility)
    int mobility_score(Board* board, Side side);
    // 5) Hybrid score
    int hybrid_score(Board* board, Side side);

    /*
     * Helper functions
     */
    // Checks if a given move is in a corner
    inline bool is_corner(int x, int y) { 
    	return ((x == 0 && (y == 0 || y == 7)) || (x == 7 && (y == 0 || y == 7)));
    };

    // Checks if a given move is on the edge
    inline bool is_edge(int x, int y) {
    	return (x == 0 || y == 0);
    };

    /*
     * Functions for minimax
     */
    // without alpha beta pruning
    int evaluateBoard(Board* board, Side side, int depth, int (Player::*score_f)(Board*, Side));
    // with alpha beta pruning
    int evaluateBoard(Board* board, Side side, int depth, int alpha, int beta, int (Player::*score_f)(Board*, Side));

public:
    Player(Side side);
    ~Player();

    void setBoard(Board* b);
    
    Move *doMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;

    // long nodes_evaluated;
    // long branches_pruned;
    // int depth_reached;
};

#endif
