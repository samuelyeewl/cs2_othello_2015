#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */

    current_board = new Board();
    s = side;

    // nodes_evaluated = 0;
    // depth_reached = 0;

    // initialize the weight table
    for (int x = 0; x < 8; ++x) {
        for (int y = 0; y < 8; ++y) {
            // first, check for corners
            if (is_corner(x, y))
                TWOD(weight_table, x, y) = CORNER_SCORE;
            // next, check for X-squares (squares diagonally adjacent to corners)
            else if ((x == 1 && (y == 1 || y == 6)) || 
                    (x == 6 && (y == 1 || y == 6)))
                TWOD(weight_table, x, y) = X_SQUARE_SCORE;
            // check for C-squares (squares horizontally adjacent to corners)
            else if (((x == 0 || x == 7) && (y == 1 || y == 6)) ||
                    ((x == 1 || x == 6) && (y == 0 || y == 7)))
                TWOD(weight_table, x, y) = C_SQUARE_SCORE;
            // check for edges
            else if (is_edge(x, y))
                TWOD(weight_table, x, y) = EDGE_SCORE;
            // check for adjacent to edge
            else if (x == 1 || x == 6 || y == 1 || y == 6)
                TWOD(weight_table, x, y) = ADJ_EDGE_SCORE;
            else
                TWOD(weight_table, x, y) = 0;
        }
    }
}

/*
 * Destructor for the player.
 */
Player::~Player() {
    delete current_board;
}

/*
 * Sets the player's board to a given state
 */
void Player::setBoard(Board* b) {
    current_board = b;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    // Simple way of determining how much time we have left
    gettimeofday(&starttime, NULL);
    int turnTime = (msLeft > 0) ? 1.1 * msLeft / (64 - current_board->countTotal()) : INT_MAX;

    // First, update the board based on opponent's move
    if (opponentsMove != NULL)
        current_board->doMove(opponentsMove, OPPSIDE(s));

    // run through all possible moves
    vector<Move*>* moves = current_board->getMoves(s);
    // if we have no moves, then return null
    if (moves->size() == 0)
        return NULL;

    // keep track of the best move
    int best_score = INT_MIN;
    Move *best_move = NULL;

    int alpha = INT_MIN + 1;
    int beta = INT_MAX - 1;

    // Initial search.
    // We use this to order the moves.
    // Need to maintain two queues
    MoveQueue* scored_moves = new MoveQueue;
    MoveQueue* scored_moves_aux = new MoveQueue;

    for (vector<Move*>::iterator it = (*moves).begin(); it != (*moves).end(); ++it)
    {
        // make a copy of the board and do the move
        Board* bcopy = current_board->copy();
        bcopy->doMove((*it), s);

        // get the score
        int val = -1 * evaluateBoard(bcopy, OPPSIDE(s), INITIAL_DEPTH, -beta, -alpha, &Player::hybrid_score);

        // updated best score
        if (val > best_score) {
            best_score = val;
            best_move = (*it);
        }
        // store the ordered moves
        MoveScore ms = make_pair((*it), val);
        scored_moves->push(ms);

        // update alpha
        if (best_score >= alpha)
            alpha = best_score;
        // no need to update beta


        delete bcopy;
    }

    // TODO replace this for loop to ensure we don't run out of time
    int depth = INITIAL_DEPTH + 1;
    gettimeofday(&currenttime, NULL);
    double msElapsed = (currenttime.tv_sec - starttime.tv_sec) * 1000 + (currenttime.tv_usec - starttime.tv_usec) / 1000;

    while (depth <= MAX_DEPTH && msElapsed < turnTime)
    {
        // run the minimax algorithm to a greater depth
        // restore alpha
        alpha = INT_MIN + 1;

        best_score = INT_MIN;
        Move* current_best = NULL;

        while (!scored_moves->empty())
        // for (vector<Move*>::iterator it = (*moves).begin(); it != (*moves).end(); ++it)
        {
            Move* current_move = (scored_moves->top()).first;
            // Move* current_move = (*it);

            // make a copy of the board and do the move
            Board* bcopy = current_board->copy();
            bcopy->doMove(current_move, s);

            // get the score
            int val = -1 * evaluateBoard(bcopy, OPPSIDE(s), depth, -beta, -alpha, &Player::hybrid_score);

            // updated best score
            if (val > best_score) {
                best_score = val;
                current_best = current_move;
            }

            // update the score of this move
            MoveScore ms = make_pair(current_move, val);
            scored_moves_aux->push(ms);

            // update alpha
            if (best_score >= alpha) 
                alpha = best_score;
            // no need to update beta

            delete bcopy;

            scored_moves->pop();
        }

        // restore the queue
        delete scored_moves;
        scored_moves = scored_moves_aux;
        scored_moves_aux = new MoveQueue;

        best_move = current_best;

        ++depth;

        gettimeofday(&currenttime, NULL);
        msElapsed = (currenttime.tv_sec - starttime.tv_sec) * 1000 + (currenttime.tv_usec - starttime.tv_usec) / 1000;
    }

    // depth_reached = (depth > depth_reached) ? depth : depth_reached;

    // do the move
    current_board->doMove(best_move, s);
    // std::cerr << "Move selected: (" << best_move->x << ", " << best_move->y << "). Score = " << best_score << std::endl;

    // garbage collection
    for (vector<Move*>::iterator it = (*moves).begin(); it != (*moves).end(); ++it)    { if ((*it) != best_move) delete (*it);}
    delete moves;
    delete scored_moves;
    delete scored_moves_aux;

    return best_move;
}

/*
 * Recursive function for minimax search
 *
 * @param board the board to be evaluated
 * @param side  the side for whom to evaluate the board
 * @param depth the remaining depth to search
 * @param score_f a scoring function
 */
int Player::evaluateBoard(Board* board, Side side, int depth, int (Player::*score_f)(Board*, Side)) {
    // ++nodes_evaluated;
    // if terminal node, return current board's score
    if (depth == 0 || board->isDone())
        return (this->*score_f)(board, side);

    // otherwise, we look at each available move, and give the maximum negative of that
    int best_score = INT_MIN;
    vector<Move*>* moves = board->getMoves(side);

    // if no moves, return current board's score
    if (moves->size() == 0)
        return (this->*score_f)(board, side);

    // otherwise create child nodes
    for (vector<Move*>::iterator it = (*moves).begin(); it != (*moves).end(); ++it)
    {
        // make a copy of the board and do the move
        Board* bcopy = board->copy();
        bcopy->doMove((*it), side);
        int val = -1 * evaluateBoard(bcopy, OPPSIDE(side), depth-1, score_f);
        if (val > best_score)
            best_score = val;

        delete (*it);
        delete bcopy;
    }

    delete moves;

    return best_score;
}

/*
 * Recursive function for minimax search with alpha-beta pruning
 *
 * @param board the board to be evaluated
 * @param side  the side for whom to evaluate the board
 * @param depth the remaining depth to search
 * @param alpha the best score we have obtained in a subtree
 * @param beta  the worst score we have obtained in a subtree
 * @param score_f a scoring function
 */
int Player::evaluateBoard(Board* board, Side side, int depth, int alpha, int beta, int (Player::*score_f)(Board*, Side)) {
    // ++nodes_evaluated;
    // if terminal node, return current board's score
    if (depth == 0 || board->isDone())
        return (this->*score_f)(board, side);
    
    // otherwise, we look at each available move, and give the maximum negative of that
    int best_score = INT_MIN;
    vector<Move*>* moves = board->getMoves(side);
    // if no moves, return current board's score
    if (moves->size() == 0)
        return (this->*score_f)(board, side);

    // otherwise create child nodes
    for (vector<Move*>::iterator it = (*moves).begin(); it != (*moves).end(); ++it)
    {
        // make a copy of the board and do the move
        Board* bcopy = board->copy();
        bcopy->doMove((*it), side);
        
        int val = -1 * evaluateBoard(bcopy, OPPSIDE(side), depth-1, -beta, -alpha, score_f);

        if (val > best_score)
            best_score = val;
        
        delete bcopy;

        // now check if our new best score is better than the minimum score in a subtree - the opponent won't let us get here.
        if (best_score >= alpha)
            alpha = best_score;

        if (alpha >= beta)
            break;
    }

    // garbage collection
    for (vector<Move*>::iterator it = (*moves).begin(); it != (*moves).end(); ++it)     delete (*it);
    delete moves;

    return best_score;
}


/*
 * Useless heuristic. For testing purposes only.
 * 
 * Returns 1 for every square
 *
 * @param   board The board to be evaluated
 * @param   side The side to evaluate the score for
 * @return  The score 
 */
int Player::constant_score(Board* board, Side side) {
    return 1;
}

/*
 * Greedy heuristic.
 *
 * Returns the difference between the number of our pieces and theirs.
 * @param   board The board to be evaluated
 * @param   side The side to evaluate the score for
 * @return  The score
 */ 
int Player::greedy_score(Board* board, Side side) {
    int diff = board->countBlack() - board->countWhite();

    return (side == BLACK) ? diff : -diff;
}

/*
 * Weighted position based heuristic.
 * 
 * Returns a score based on the sum of the positions on the board.
 * Weights corners and edges highly.
 * Gives negative weight to squares adjacent to corners and edges.
 * Final score is the difference between the score for our side and the opposite side
 */
int Player::positional_score(Board* board, Side side) {
    // add slight dynamicity by modifying the weight table
    int mod_weight_table[8*8];
    memcpy(mod_weight_table, weight_table, 64*sizeof(int));

    // if corners are occupied, then the pieces adjacent to it will be stable
    if (board->occupied(0, 0)) {
        TWOD(mod_weight_table, 0, 1) = MOD_C_SCORE;
        TWOD(mod_weight_table, 1, 0) = MOD_C_SCORE;
        TWOD(mod_weight_table, 1, 1) = MOD_X_SCORE;
    }
    if (board->occupied(0, 7)) {
        TWOD(mod_weight_table, 0, 6) = MOD_C_SCORE;
        TWOD(mod_weight_table, 1, 7) = MOD_C_SCORE;
        TWOD(mod_weight_table, 1, 6) = MOD_X_SCORE;
    }
    if (board->occupied(7, 0)) {
        TWOD(mod_weight_table, 6, 0) = MOD_C_SCORE;
        TWOD(mod_weight_table, 7, 1) = MOD_C_SCORE;
        TWOD(mod_weight_table, 6, 1) = MOD_X_SCORE;
    }
    if (board->occupied(7, 7)) {
        TWOD(mod_weight_table, 7, 6) = MOD_C_SCORE;
        TWOD(mod_weight_table, 6, 7) = MOD_C_SCORE;
        TWOD(mod_weight_table, 6, 6) = MOD_X_SCORE;
    }

    int score = 0;

    for (int x = 0; x < 8; ++x)
    {
        for (int y = 0; y < 8; ++y)
        {
            if (board->get(side, x, y))
                score += TWOD(mod_weight_table, x, y);
            else if (board->get(OPPSIDE(side), x, y))
                score -= TWOD(mod_weight_table, x, y);
        }
    }

    return score;
}

/*
 * Mobility based heuristic.
 * 
 * Evaluates move purely based on the resultant mobility available to the player
 * Returns the difference between the number of available moves for us and the number
 * of available moves for the opponent.
 */
int Player::mobility_score(Board* board, Side side) {
    return board->countMoves(side) - board->countMoves(OPPSIDE(side));
}

/*
 * Hybrid score based on the position, mobility and greedy algorithms.
 *
 * Highest priority given to the positional score.
 * Mobility score is added to the positional score, modified by some multiplier.
 * Increased priority given to the greedy score at the end of the game.
 */
int Player::hybrid_score(Board* board, Side side) {
    // multiplier on the mobility score
    double MOBILITY_MULTIPLIER = 0.1;
    // number of turns before we start using greedy
    int GREEDY_TURNS = 5;
    double GREEDY_MULTIPLIER = 10;

    // do not allow evaporation at any cost
    if (board->count(side) < 2)
        return INT_MIN + 1;

    // Get the positional_score first
    int ps = positional_score(board, side);
    int ms = mobility_score(board, side);
    // double score = ps + MOBILITY_MULTIPLIER * ms;
    double score;
    if (ps == 0)
        score = MOBILITY_MULTIPLIER * ms;
    else
        score = ps + (ps >= 0 ? ps : -ps) * MOBILITY_MULTIPLIER * ms;

    // std::cerr << "Positional score: " << ps << ", Mobility score: " << ps * 0.3 * ms << std::endl;

    // Add the greedy score if we are near the end-game
    int empty_spaces = 64 - board->countBlack() - board->countWhite();
    if (empty_spaces < GREEDY_TURNS) {
        score += GREEDY_MULTIPLIER * (GREEDY_TURNS - empty_spaces) * greedy_score(board, side);
    }

    return int(score);

}
