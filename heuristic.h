#ifndef __HEURISTIC_H__
#define __HEURISTIC_H__

#define CORNER_SCORE 100
#define EDGE_SCORE 10
#define X_SQUARE_SCORE -50
#define C_SQUARE_SCORE -20
#define ADJ_EDGE_SCORE -10

#define MOD_X_SCORE 50
#define MOD_C_SCORE 50

#endif //__HEURISTIC_H__